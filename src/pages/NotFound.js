import React from "react";
import Navigation from "../components/Navigation";
import "../styles/notFound.css";

const NotFound = () => {
  return (
    <div>
      <h1>Erreur : Page non trouvée revenir à l'accueil</h1>
      <Navigation />
    </div>
  );
};

export default NotFound;
