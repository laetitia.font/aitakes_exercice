import React from "react";
import "../styles/detail.css";
import { Link } from "react-router-dom";
import DetailBook from "../components/DetailBook";

// Création de la fenêtre modale
const Modale = ({ revele, cache }) =>
  revele ? (
    <React.Fragment>
      <div className="overLayModal" />
      <div className="wrapper">
        <button type="button" onClick={cache}>
          <Link className="link" to="/gallery">
            Retour
          </Link>
        </button>
        <div className="modal">
          <DetailBook />
        </div>
      </div>
    </React.Fragment>
  ) : null;

export default Modale;
