import React from "react";
import Navigation from "../components/Navigation";
import Books from "../components/Books";
import "../styles/photoGallery.css";

const PhotoGallery = () => {
  return (
    <div>
      <h1>Galerie de Livres</h1>
      <Books />
      <Navigation />
    </div>
  );
};

export default PhotoGallery;
