import React from "react";
import Navigation from "../components/Navigation";
import Slider from "../components/Slider";
import "../styles/home.css";

const Home = () => {
  return (
    <div>
      <Slider />

      <Navigation />
    </div>
  );
};

export default Home;
