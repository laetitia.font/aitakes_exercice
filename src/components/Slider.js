import "../styles/components/slider.css";
import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

function Slider() {
  const baseUrl = "http://react-responsive-carousel.js.org/assets/",
    datas = [
      {
        id: 1,
        img: `${baseUrl}4.jpeg`,
        title: "Arbres",
        text: `Les arbres sont de grands sages...bien ancrés dans le sol, ils sont à l'écoute de la terre, mais cela ne les empêche pas d'avoir la tête dans les nuages et d'écouter les histoires du vent...et toujours vouloir aller plus haut, vers la lumière.
        Michel Tournier`,
      },
      {
        id: 2,
        img: `${baseUrl}5.jpeg`,
        title: "Ciel",
        text: `Ne sois pas plus haut que le ciel, plus prophétique que les prophètes, demeure juste un homme car au fond et souvent, on est des faibles créatures incapables de résister à un mal de dents.
        Mostefa Khellaf`,
      },
      {
        id: 3,
        img: `${baseUrl}6.jpeg`,
        title: "Fleur",
        text: `La renaissance est une fleur qui s'épanouit naturellement, dès lors que l'âme retrouve sa liberté d'être.
        Joëlle Laurencin`,
      },
    ];

  return (
    <Carousel
      autoPlay
      interval={6000}
      infiniteLoop
      thumbWidth={120}
      showIndicators={false}
      showStatus={false}
    >
      {datas.map((slide) => (
        <div key={slide.id}>
          <img src={slide.img} alt="img" />
          <div className="overLay">
            <h1 className="overLay__Title">{slide.title}</h1>
            <p className="overLay__text">{slide.text}</p>
          </div>
        </div>
      ))}
    </Carousel>
  );
}

export default Slider;
