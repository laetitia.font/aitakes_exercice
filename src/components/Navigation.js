import React from "react";
import { NavLink } from "react-router-dom";
import "../styles/components/navigation.css";

const Navigation = () => {
  return (
    <div className="navigation">
      <NavLink className="home" exact to="/">
        Accueil
      </NavLink>
      <NavLink className="list" exact to="/gallery">
        Liste
      </NavLink>
    </div>
  );
};

export default Navigation;
