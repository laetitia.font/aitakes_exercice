import { useState } from "react";

// Création de la fonction toogle qui affiche la fenêtre modale
const LogicModal = () => {
  const [revele, changeRevele] = useState(false);

  function toggle() {
    changeRevele(!revele);
  }

  return {
    revele,
    toggle,
  };
};

export default LogicModal;
