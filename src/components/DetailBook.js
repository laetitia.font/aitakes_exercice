import React from "react";
// import axios from "axios";
// import { useState, useEffect } from "react";
import "../styles/components/detailBook.css";
import Books from './Books';

// export function ChildBook ({children}) {
//   console.log(React.Children.toArray(children));
// }

const DetailBook = ({  }) => {
  // Défilement du scroll vers le haut
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // TODO: Fonction à terminer en incluant les données de l'API,en fonction de l'id (rank)
  return (
    <>
      <section className="books">
        <div className="detail">
          <div>
            <img src="img/logo192.png" className="imgDetail" alt="imgDetail" />
          </div>
          <h1>Ici le titre du Livre</h1>
          <p>Description du livre</p>
          <div>
            <p className="author">Auteur : Laëtitia</p>
            {/* <p>{Books.author}</p> */}
          </div>
        </div>
      </section>
    </>
  );
};

export default DetailBook;