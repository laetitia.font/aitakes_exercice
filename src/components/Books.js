import React from "react";
import LogicModal from "../components/LogicModal";
import Modale from "../pages/Detail";
import { useState, useEffect } from "react";
import axios from "axios";
import "../styles/components/books.css";

const Books = () => {

  const [books, setBooks] = useState([]);
  const { revele, toggle } = LogicModal();

  //   Appel de l'API à l'aide d'une fonction Asynchrone
  useEffect(() => {
    const fetchBooks = async () => {
      const res = await axios.get(
        `https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=${process.env.REACT_APP_BOOKS_API_KEY}`
      );
      setBooks(res.data.results.books);
    };

    fetchBooks();
  }, []);

  // Retourne la liste des livres de l'API
  return (
    <>
      <section className="books">
        {books.map((book) => {
          const { rank, title, book_image, author, description } = book;

          return (
            <article key={rank}>
              <div>
                <button onClick={toggle}>
                  <img src={book_image} alt={title} />
                </button>
                <Modale
                  revele={revele}
                  cache={toggle}
                />
              </div>
            </article>
          );
        })}
      </section>
    </>
  );
};

export default Books;
